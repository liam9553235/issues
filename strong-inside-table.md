See if this renders properly:

```
UID | Use if severity | Status
--- | --------------- | ---
9   |                 |
6   | **NIWA**HD      | Enabled
7   | NIW**A**HD      | Enabled
8   | NIW**A**HD      | Enabled
1   | NIW**AHD**      | Enabled

| UID | Use if severity | Status |
| --- | --------------- | --- |
| 9   |                 | |
| 6   | **NIWA**HD      | Enabled |
| 7   | NIW**A**HD      | Enabled |
| 8   | NIW**A**HD      | Enabled |
| 1   | NIW**AHD**      | Enabled |
```

Rendering:

UID | Use if severity | Status
--- | --------------- | ---
9   |                 |
6   | **NIWA**HD      | Enabled
7   | NIW**A**HD      | Enabled
8   | NIW**A**HD      | Enabled
1   | NIW**AHD**      | Enabled

| UID | Use if severity | Status |
| --- | --------------- | --- |
| 9   |                 | |
| 6   | **NIWA**HD      | Enabled |
| 7   | NIW**A**HD      | Enabled |
| 8   | NIW**A**HD      | Enabled |
| 1   | NIW**AHD**      | Enabled |

Relevant documentation: https://docs.gitlab.com/ee/user/markdown.html#tables

Properly rendered HTML is in [strong-inside-table.html](strong-inside-table.html)

[Babelmark 2](http://johnmacfarlane.net/babelmark2) does [comparative renderings](http://johnmacfarlane.net/babelmark2/?normalize=1&text=UID+%7C+Use+if+severity+%7C+Status%0A---+%7C+---------------+%7C+---%0A9+++%7C+++++++++++++++++%7C%0A6+++%7C+**NIWA**HD++++++%7C+Enabled%0A7+++%7C+NIW**A**HD++++++%7C+Enabled%0A8+++%7C+NIW**A**HD++++++%7C+Enabled%0A1+++%7C+NIW**AHD**++++++%7C+Enabled) of many markdown engines. Click the `Preview` link there to show the rendered tables after all `code` has been returned.

Rendered was done by this [pandoc](http://pandoc.org/) command:

```
pandoc -s strong-inside-table.md -o strong-inside-table.html
```

This pandoc version was used:

```
# pandoc --version
pandoc 1.19.2.1
Compiled with pandoc-types 1.17.0.5, texmath 0.9, skylighting 0.1.1.4
Default user data directory: /Users/jeroenp/.pandoc
Copyright (C) 2006-2016 John MacFarlane
Web:  http://pandoc.org
This is free software; see the source for copying conditions.
There is no warranty, not even for merchantability or fitness
for a particular purpose.
```
