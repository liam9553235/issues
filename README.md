A more or less complete list of GitLab issues I reported or commented on.

- Destilled from https://gitlab.com/wiert
- Reverse chronological order

Projects: https://gitlab.com/gitlab-com

# Reported

- [https://monitor.gitlab.net/ has wrong TLS/SSL certificate (#3249) · Issues · GitLab.com / infrastructure](https://gitlab.com/gitlab-com/infrastructure/issues/3249)
  - originally [https://monitor.gitlab.net/ has wrong TLS/SSL certificate (#2733) · Issues · GitLab.com / GitLab.com Support Tracker](https://gitlab.com/gitlab-com/support-forum/issues/2733)   
- [https://feedback.gitlab.com/ has expired TLS/SSL certificate (#2732) · Issues · GitLab.com / GitLab.com Support Tracker](https://gitlab.com/gitlab-com/support-forum/issues/2732)
- [Cannot search over all branches from the top navigation bar search functionality (#40243) · GitLab.org / GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues/40243)
- [Printing markdown that fits exactly on one A4 page appends empty second page. (#40239) · GitLab.org / GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues/40239)
- [Please enable PlantUML rendering on gitlab.com both for standalone plantuml files and inside markdown plantuml code blocks (#2163) · GitLab.com / infrastructure](https://gitlab.com/gitlab-com/infrastructure/issues/2163)
- Closed: [Please enable PlantUML rendering on gitlab.com both for standalone plantuml files and inside markdown plantuml code blocks (#2041) · GitLab.com / GitLab.com Support Tracker](https://gitlab.com/gitlab-com/support-forum/issues/2041)
- [One of the team members cannot access a new project; other team members can (#2031) · GitLab.com / GitLab.com Support Tracker](https://gitlab.com/gitlab-com/support-forum/issues/2031)
- Closed: [Commit on a new private repository not visible in the GitLab web UI (#1980) GitLab.com / GitLab.com Support Tracker](https://gitlab.com/gitlab-com/support-forum/issues/1980)
- Closed: ["slash commands" link throws "500 server error" (#1978) GitLab.com / GitLab.com Support Tracker](https://gitlab.com/gitlab-com/support-forum/issues/1978)
- [Markdown rendering of double indented lists fails (#33471) GitLab.org / GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues/33471)
- Closed: [Markdown and reStructuredText files get rendered, but not while printing (#33468) GitLab.org / GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues/33468)
- [Rendering backslash escaped markdown fails (#33430) GitLab.org / GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues/33430)
- [co-owner of group doesn't see the subgroups I created, but within the subgroup can see he is member (#33352) GitLab.org / GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues/33352)
- Closed: [1 error prohibited this user from being saved: Email confirmation doesn't match Email (#33354) GitLab.org / GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues/33354)
- Closed: [The GitLab.com outgoing mail server 198.61.254.240 is blacklisted at UCEPROTECT Blacklist Policy LEVEL 2 (#33365) GitLab.org / GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues/33365)

# Commented on

- Closed: [Mailgun suggests we set up a PTR record for our Mailgun dedicated IP (#940) GitLab.com / infrastructure](https://gitlab.com/gitlab-com/infrastructure/issues/940#note_32026292)
- Closed: [Mailgun IP (subnet) on uceprotect blacklist (#1929) GitLab.com / infrastructure](https://gitlab.com/gitlab-com/infrastructure/issues/1929#note_31725971)
- [Error when trying to signup using an email address with uppercase letters (#27898) GitLab.org / GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues/27898#note_31702063)
- [Build SUSE packages (#1081) GitLab.org / omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/1081#note_19683813)

--jeroen