See if this renders properly:

```
- [code\\](code)
- [documentation\\](documentation)
```

Rendering:

- [code\\](code)
- [documentation\\](documentation)

Properly rendered HTML is in [backslash-escaping.html](backslash-escaping.html)

Rendered was done by this [pandoc](http://pandoc.org/) command:

```
pandoc -s backslash-escaping.md -o backslash-escaping.html
```

This pandoc version was used:

```
# pandoc --version
pandoc 1.19.2.1
Compiled with pandoc-types 1.17.0.5, texmath 0.9, skylighting 0.1.1.4
Default user data directory: /Users/jeroenp/.pandoc
Copyright (C) 2006-2016 John MacFarlane
Web:  http://pandoc.org
This is free software; see the source for copying conditions.
There is no warranty, not even for merchantability or fitness
for a particular purpose.
```
